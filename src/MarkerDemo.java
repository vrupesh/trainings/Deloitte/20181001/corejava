import com.guruofjava.deloitte2.basics.Marker;

public class MarkerDemo {
	public static void main(String[] args) {
		Marker m = new Marker("Camlin", Marker.GREEN, 25);
		
		System.out.println(m.getColor());
		
		m.setColor(Marker.RED);
		System.out.println(m.getColor());
		
		m.setColor("Brown");
		System.out.println(m.getColor());
	}
	
	public static void main4(String[] args) {
		Marker m1 = new Marker();
		
		System.out.println(m1.getPrice());
		m1.setPrice(18);
		System.out.println(m1.getPrice());
		m1.setPrice(-4);
		System.out.println(m1.getPrice());
	}
	
	/*public static void main3(String[] args) {
		System.out.println(Marker.category);
		
		Marker m1 = new Marker("Reynolds", "Blue", 28);
		Marker m2 = new Marker("Cello", "Green", 30);
		
		System.out.println(m1.color);
		System.out.println(m2.color);
		
		System.out.println(m1.category);
		System.out.println(m2.category);
		
		m1.color = "Black";
		m2.color = "Red";
		
		m1.category = "Stationary";
		m2.category = "Writing Instruments";
		
		System.out.println(m1.color);
		System.out.println(m2.color);
		
		System.out.println(m1.category);
		System.out.println(m2.category);
	}
	
	public static void main2(String[] args) {
		Marker m1 = new Marker();
		
		m1.write("James Gosling");
		m1.write(289);
		m1.write(126.45);
		
	}
	
	public static void main1(String[] args) {
		Marker m1 = new Marker("Camlin", "Black", 20);
		
		System.out.println(m1.BRAND);
		System.out.println(m1.color);
		//System.out.println(m1.price);
		System.out.println(m1.getPrice());
		
		//m1.price = -10;
		m1.setPrice(-10);
		System.out.println(m1.getPrice());
	}*/
}
