import java.util.Calendar;

public class MethodsDemo {
	public static void main(String[] args) {
		int a = 20, b = 30, c = 25;
		
		int d = max(c, max(a, b));
		System.out.println(d);
		
		System.out.println("Today is " + today());
	}
	
	private static int today() {
		Calendar c = Calendar.getInstance();
		return c.get(Calendar.DATE);
	}
	
	public static int max(int x, int y) {
		if(x > y) {
			return x;
		}else {
			return y;
		}
	}
	
	
	public static void main1(String[] args) {
		printLine(55, '@');
		printLine(50);
		System.out.println("-----------------------------------------");
		System.out.println("James Gosling created Java");
		System.out.println("-----------------------------------------");

		System.out.println("Created at Sun Microsystems");
		printLine();
		printLine(20);
		printLine(55, '=');
	}

	public static void printLine(int length, char c) {
		for (int i = 0; i < length; i++) {
			System.out.print(c);
		}
		System.out.println();
	}

	public static void printLine(int length) {
		for (int i = 0; i < length; i++) {
			System.out.print("-");
		}
		System.out.println();
	}

	public static void printLine() {
		System.out.println("-----------------------------------------");
	}
}
