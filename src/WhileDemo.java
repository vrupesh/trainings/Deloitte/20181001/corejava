
public class WhileDemo {
	public static void main(String[] args) {
		int i = 20;

		do {
			System.out.println(10 + " x " + i + " = " + i * 10);
			
			i++;
		} while (i <= 10);
		
		
	}

	public static void main1(String[] args) {
		int i = 5;

		while (i <= 10) {
			System.out.println(10 + " x " + i + " = " + i * 10);

			i++;
		}
	}
}
