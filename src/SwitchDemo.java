
public class SwitchDemo {
	public static void main(String[] args) {
		int month = 3;

		switch (month) {
		case 3:
		case 1:
			System.out.println("31 Days");
			break;
		case 2:
			System.out.println("28/29 Days");
			break;
		default:
			System.out.println("No Match");
		}
	}
	
	public static void main1(String[] args) {
		int month = 2;

		switch (month) {
		case 1:
			System.out.println("31 Days");
			break;
		case 2:
			System.out.println("28/29 Days");
			break;
		case 3:
			System.out.println("31 Days");
			break;
		default:
			System.out.println("No Match");
		}
	}
}
