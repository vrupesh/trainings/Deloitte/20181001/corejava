package com.guruofjava.deloitte2.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Algorithms {
	public static void main(String[] args) {
		List t1 = new ArrayList();
		
		t1.add(56);
		t1.add(49);
		t1.add(36);
		t1.add(21);
		t1.add(25);
		t1.add(35);
		t1.add(8);
		t1.add(69);
		t1.add(78);
		t1.add(29);
		
		System.out.println(t1);
		
		//Collections.sort(t1);
		//Collections.shuffle(t1);
		//System.out.println(Collections.max(t1));
		//System.out.println(Collections.min(t1));
		//Collections.rotate(t1, 4);
		//Collections.reverse(t1);
		Collections.swap(t1, 4, 7);
		System.out.println(t1);
	}
}
