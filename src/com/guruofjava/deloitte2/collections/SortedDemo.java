package com.guruofjava.deloitte2.collections;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

import com.guruofjava.deloitte2.basics.Marker;

public class SortedDemo {
	public static void main(String[] args) {
		Marker m1 = new Marker("Camlin", "Blue", 22);
		Marker m2 = new Marker("Montex", Marker.BLACK, 28);
		
		System.out.println(m1);
		System.out.println(m2);
		
		Set s1 = new TreeSet();
		s1.add(m1);
		s1.add(m2);
		System.out.println(s1);
	}
	
	public static void main1(String[] args) {
		Set s1 = new TreeSet();
		
		s1.add("James");
		s1.add(1996);
		s1.add("Johnson");
		
		System.out.println(s1);
	}
}
