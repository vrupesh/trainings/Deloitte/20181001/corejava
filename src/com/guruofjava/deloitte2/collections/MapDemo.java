package com.guruofjava.deloitte2.collections;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

public class MapDemo {
	public static void main(String[] args) {
		SortedMap m1 = new TreeMap();

		m1.put("James", "Gosling");
		m1.put("java", 1996);
		m1.put("Sea", 1971);
		m1.put("Codd", "Oracle");
		
		System.out.println(m1);
		
		Map m2 = m1.headMap("Sea");
		System.out.println(m2);
	}

	public static void main1(String[] args) {
		Map m1 = new LinkedHashMap();

		System.out.println(m1.isEmpty());
		System.out.println(m1.size());

		m1.put("James", "Gosling");
		m1.put("java", 1996);
		m1.put("Sea", 1971);

		System.out.println(m1);

		System.out.println(m1.containsKey("john"));
		System.out.println(m1.containsKey("java"));
		System.out.println(m1.containsValue("Gosling"));
		System.out.println(m1.containsValue(1971));

		System.out.println(m1.get("java"));

		Set keys = m1.keySet();
		System.out.println(keys);

		Collection values = m1.values();
		System.out.println(values);

	}
}
