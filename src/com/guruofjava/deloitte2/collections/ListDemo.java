package com.guruofjava.deloitte2.collections;

import java.util.LinkedList;
import java.util.List;

public class ListDemo {
	public static void main(String[] args) {
		List t1 = new LinkedList();
		
		t1.add("One");
		t1.add("two");
		t1.add("three");
		t1.add(10);
		t1.add("four");
		t1.add(20);
		t1.add("James");
		t1.add("Bloch");
		
		System.out.println(t1);
		
		t1.add(6, "Johnson");
		System.out.println(t1);
		
		System.out.println(t1.get(4));
		
		t1.remove("James");
		System.out.println(t1);
		
		//t1.remove(20);
		t1.remove(new Integer(20));
		System.out.println(t1);
	}
}
