package com.guruofjava.deloitte2.collections;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetDemo {
	public static void main(String[] args) {
		Set s1 = new TreeSet();
		s1.add("One");
		s1.add("Two");
		s1.add("Three");
		s1.add("Four");
		
		System.out.println(s1);
	}
	
	public static void main2(String[] args) {
		HashSet s1 = new LinkedHashSet();
		s1.add("One");
		s1.add("Two");
		s1.add("Three");
		s1.add("Four");
		System.out.println(s1);
		
		HashSet s2 = new HashSet();
		s2.add("Two");
		s2.add("Five");
		s2.add("Eight");
		s2.add("Three");
		System.out.println(s2);
		
		System.out.println();
		
		//s1.addAll(s2);
		//s1.removeAll(s2);
		s1.retainAll(s2);
		System.out.println(s1);
		System.out.println(s1.containsAll(s2));
		
		s1.remove("Two");
		System.out.println(s1);
		
		Object[] ta = s2.toArray();
	}

	public static void main1(String[] args) {
		HashSet s1 = new HashSet();

		s1.add("One");
		s1.add("Two");
		s1.add("Three");
		s1.add("Four");

		System.out.println(s1);

		Iterator it = s1.iterator();

		while (it.hasNext()) {
			String temp = (String) it.next();
			System.out.println(temp);

			if (temp.equals("Three")) {
				it.remove();
			}
		}

		System.out.println(s1);

		System.out.println(s1.size());
		System.out.println(s1.isEmpty());
		s1.clear();
		System.out.println(s1.isEmpty());

	}
}
