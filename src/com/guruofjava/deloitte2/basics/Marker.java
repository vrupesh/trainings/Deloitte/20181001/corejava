package com.guruofjava.deloitte2.basics;

import com.guruofjava.deloitte2.exceptions.MarkerColorNotSupportedException;

public class Marker {
	public final String BRAND;
	private String color;
	private double price;

	public final static String BLUE;
	public final static String BLACK;
	public final static String GREEN;
	public final static String RED;

	static {
		BLUE = "Blue";
		BLACK = "Black";
		GREEN = "Green";
		RED = "Red";
	}

	public static String category = "Pens";

	public String toString() {
		return "Marker[Brand: " + BRAND + ", Color: " + color + ", Price: " + price + "]";
	}
	
	public String getColor() {
		return color;
	}

	public void setColor(String color) throws MarkerColorNotSupportedException {
		switch (color) {
		case "Blue":
		case "Black":
		case "Green":
		case "Red":
			this.color = color;
			break;
		default:
			//throw new IllegalArgumentException("Invalid Marker Color: " + color);
			throw new MarkerColorNotSupportedException("For color: " + color);
		}
	}

	public void write(double input) {
		System.out.println(input);
	}

	public void write(int input) {
		System.out.println(input);
	}

	public void write(String input) {
		System.out.println(input);
	}

	public void setPrice(double p) throws IllegalArgumentException {
		// price = p;
		if (p > 0) {
			price = p;
		} else {
			// System.out.println("Invalid Marker Price: " + p);

			throw new IllegalArgumentException("Invalid price: " + p);
		}
	}

	public double getPrice() {
		return price;
	}

	public Marker() {
		BRAND = "Reynolds";
		color = "Blue";
		price = 25.0;
	}

	public Marker(String b) {
		BRAND = b;
		color = "Blue";
		price = 25.0;
	}

	public Marker(String b, String c, double p) {
		BRAND = b;
		color = c;
		price = p;
	}
}
