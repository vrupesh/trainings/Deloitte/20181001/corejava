package com.guruofjava.deloitte2.inheritance;

public class Cat extends Animal implements Hunter, Runner, Jumper{

	@Override
	public void jump(int length) {
		System.out.println("Cat jumping: " + length + " distance");
	}

	@Override
	public void run(int distance) {
		System.out.println("Cat running: " + distance + " distance");
	}

	@Override
	public void hunt() {
		System.out.println("Cat is hunting");
	}

	@Override
	public void move(int distance) {
		System.out.println("Cat is moving: " + distance + " distance");
	}
	
}
