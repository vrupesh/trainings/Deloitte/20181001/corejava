package com.guruofjava.deloitte2.inheritance;

public interface Hunter {
	void hunt();
}
