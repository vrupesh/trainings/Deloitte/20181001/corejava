package com.guruofjava.deloitte2.inheritance;

public interface Runner {
	public void run(int distance);
}
