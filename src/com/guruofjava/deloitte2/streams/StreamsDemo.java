package com.guruofjava.deloitte2.streams;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Objects;

public class StreamsDemo {
	public static void main(String[] args) {
		FileInputStream in = null;
		FileOutputStream out = null;

		try {
			in = new FileInputStream(
					"C:\\Users\\rkvod\\RKV\\TECHs\\TRAININGS\\Deloitte\\20181001\\html&Css\\mystyles.css");
			out = new FileOutputStream(
					"C:\\Users\\rkvod\\RKV\\TECHs\\TRAININGS\\Deloitte\\20181001\\html&Css\\mystyles1.css");

			int t;

			while ((t = in.read()) != -1) {
				System.out.print((char) t);
				out.write(t);
			}

		} catch (FileNotFoundException fnfe) {

		} catch (IOException ioe) {

		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {

				}
			}
			if(Objects.nonNull(out)) {
				try {
					out.close();
				}catch(IOException ioe) {
					
				}
			}
		}
	}

	public static void main1(String[] args) {
		FileInputStream in = null;

		try {
			in = new FileInputStream(
					"C:\\Users\\rkvod\\RKV\\TECHs\\TRAININGS\\Deloitte\\20181001\\html&Css\\mystyles.css");

			int t;

			while ((t = in.read()) != -1) {
				System.out.print((char) t);
			}

		} catch (FileNotFoundException fnfe) {

		} catch (IOException ioe) {

		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {

				}
			}
		}
	}
}
