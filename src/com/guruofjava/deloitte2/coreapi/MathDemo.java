package com.guruofjava.deloitte2.coreapi;

public class MathDemo {
	public static void main(String[] args) {
		System.out.println(Math.PI);
		System.out.println(Math.E);
		
		System.out.println(Math.max(345, 278));
		System.out.println(Math.min(345, 278));
		
		System.out.println(Math.floor(36.9));
		System.out.println(Math.ceil(36.9));
		System.out.println(Math.round(36.499));
		
		System.out.println(Math.sqrt(24));
		System.out.println(Math.pow(3, 4));
		System.out.println(Math.abs(220));
		
		System.out.println(Math.random());
		
		for(long i=0, j; i<10; i++) {
			j = Math.round(50 + Math.random() * 50);
			System.out.print(j + " ");
		}
	}
}
