package com.guruofjava.deloitte2.coreapi;

public class StringDemo {
	public static void main(String[] args) {
		String s1 = "java";
		String s2 = "java";
		
		String s3 = new String("java");
		
		
		if(s1 == s2) {
			System.out.println("s1 & s2 are same");
		}else {
			System.out.println("s1 & s2 are not same");
		}
		
		System.out.println(s1);
		System.out.println(s2);
		System.out.println(s3);
		
		s3 = s3.intern();
		if(s1 == s3) {
			System.out.println("s1 & s3 are same");
		}else {
			System.out.println("s1 & s3 are not same");
		}
		
		System.out.println(s1.hashCode());
		System.out.println(s2.hashCode());
		System.out.println(s3.hashCode());
		System.out.println();
		
		System.out.println(System.identityHashCode(s1));
		System.out.println(System.identityHashCode(s2));
		System.out.println(System.identityHashCode(s3));
	}
	
	public static void main2(String[] args) {
		String a = "java";
		String b = "Java";
		
		System.out.println(a.equals(b));
		System.out.println(a.equalsIgnoreCase(b));
		
		System.out.println(a.contains("aa"));
		System.out.println(a.startsWith("ja"));
		System.out.println(a.startsWith("av", 1));
		System.out.println(a.endsWith("eva"));
		System.out.println(a.endsWith("ava"));
		
		String s1 = " ";
		System.out.println(s1.isEmpty());
		System.out.println(s1.trim().isEmpty());
	}
	
	public static void main1(String[] args) {
		String s1 = "Java Programming Language";
		
		System.out.println(s1.length());
		System.out.println(s1);
		System.out.println(s1.charAt(9));
		System.out.println(s1.indexOf('a'));
		System.out.println(s1.lastIndexOf('a'));
		System.out.println(s1.toUpperCase());
		System.out.println(s1);
		System.out.println(s1.hashCode());
		
		s1 = s1 + ", created by James";
		System.out.println(s1);
		System.out.println(s1.hashCode());
		
		System.out.println(s1.substring(10));
		System.out.println(s1.substring(10, 12));
	}
}
