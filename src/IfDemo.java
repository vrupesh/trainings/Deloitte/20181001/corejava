
public class IfDemo {
	public static void main(String[] args) {
		int marks = 50;

		if (marks >= 75) {
			System.out.println("Distinction");
		} else if (marks >= 60) {
			System.out.println("First Class");
		} else if (marks >= 50) {
			System.out.println("Second Class");
		} else if (marks >= 40) {
			System.out.println("Third Class");
		} else
			System.out.println("Failed");
	}

	public static void main5(String[] args) {
		int marks = 50;

		if (marks >= 75) {
			System.out.println("Distinction");
		} else {
			if (marks >= 60) {
				System.out.println("First Class");
			} else {
				if (marks >= 50) {
					System.out.println("Second Class");
				} else {
					if (marks >= 40) {
						System.out.println("Third Class");
					} else {
						System.out.println("Failed");
					}
				}
			}
		}
	}

	public static void main4(String[] args) {
		int marks = 50;

		if (marks >= 75) {
			System.out.println("Distinction");
		}

		if (marks >= 60 && marks < 75) {
			System.out.println("First Class");
		}

		if (marks >= 50 && marks < 60) {
			System.out.println("Second Class");
		}

		if (marks >= 40 && marks < 50) {
			System.out.println("Third Class");
		}

		if (marks < 40) {
			System.out.println("Failed");
		}
	}

	public static void main3(String[] args) {
		int i = 2;

		if (i == 2) {
			System.out.println("Two");
		} else {
			System.out.println("Not Two");
		}
	}

	public static void main2(String[] args) {
		int i = 4;

		System.out.println("One");

		// if(true)
		if (i == 2) {
			System.out.println("Two");
			System.out.println("Second");
		}

		System.out.println("Three");
	}

	public static void main1(String[] args) {
		System.out.println("One");
		System.out.println("Two");
		System.out.println("Three");
	}
}
